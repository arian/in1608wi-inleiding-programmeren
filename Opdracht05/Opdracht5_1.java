//	Practicum IN1608WI	Opdracht 5
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht5;

public class Opdracht5_1 {

	public static void main(String[] argv){

		int[] seq2 = {1, 2};
		int[] seq6 = {4, 7, 2, -1, 5, 7};

		System.out.print("De inhoud van seq2 is: ");
		println(seq2);

		verwissel(seq2);
		System.out.print("Na uitvoering van verwissel(seq2) is de inhoud van seq2: ");
		println(seq2);

		int[] res = kopieer(seq2);
		System.out.print("De kopie van seq2 is res, de inhoud van res is:");
		println(res);

		System.out.println("res verwijst niet naar seq2: " + (seq2 != res) + "\n");

		System.out.print("De inhoud van seq6 is: ");
		println(seq6);
		
		System.out.print("Na uitvoering van roteer(seq6) is de inhoud van seq6: ");
		roteer(seq6);
		println(seq6);

		System.out.print("Na uitvoering van roteer(seq6, 3) is de inhoud van seq6: ");
		roteer(seq6, 3);
		println(seq6);

	}

	/**
	 * Prints an array on one line
	 */
	public static void println(int[] seq){
		String result = "";
		for (int i = 0; i < seq.length; i++){
			result += seq[i] + " ";
		}
		System.out.print(result + "\n");
	}

	/**
	 * Swaps the first two elements in the array
	 */
	public static void verwissel(int[] seq){
		if (seq.length >= 2){
			int tmp = seq[1];
			seq[1] = seq[0];
			seq[0] = tmp;
		}
	}

	/**
	 * Copies all the elements of one array into a new one
	 */
	public static int[] kopieer(int[] seq){
		int n = seq.length;
		int[] res = new int[n];
		for (int i = 0; i < n; i++){
			res[i] = seq[i];
		}
		return res;
	}

	/**
	 * Shift all the elements one place to the right, and places the last element
	 * as first one
	 * @param seq
	 */
	public static void roteer(int[] seq){
		int l = seq.length;
		if (l > 1){
			int tmp = seq[l - 1];
			for (int i = l; i > 1; i--){
				seq[i - 1] = seq[i - 2];
			}
			seq[0] = tmp;
		}
	}

	/**
	 * Shifts all the elements n places to the right, and places the last n
	 * elements at the beginning of the array
	 * @param seq
	 * @param n
	 */
	public static void roteer(int[] seq, int n){
		while (n-- > 0)
			roteer(seq);
	}

}
