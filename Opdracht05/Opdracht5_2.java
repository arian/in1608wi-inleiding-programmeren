//	Practicum IN1608WI	Opdracht 5
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht5;

public class Opdracht5_2 {

	public static void main(String[] argv){

		int[] seq = {7,5,7,2,11,-4,5,9,10,2};

		System.out.println("De inhoud van seq is: ");
		println(seq);

		System.out.println("De waarde van max(seq) is: " + max(seq));
		System.out.println("De waarde van index(seq, 3) is: " + index(seq, 3));
		System.out.println("De waarde van index(seq, 5) is: " + index(seq, 5));
		System.out.println("De waarde van bevat(seq, 3) is: " + bevat(seq, 3));
		System.out.println("De waarde van bevat(seq, 5) is: " + bevat(seq, 5));
		System.out.println("isPriem(3) is: " + isPriem(3));
		System.out.println("isPriem(4) is: " + isPriem(4));
		System.out.println("telPriemgetallen(seq) levert: " + telPriemgetallen(seq));
		System.out.println("de priemgetallen in seq zijn: ");
		println(priemgetallenIn(seq));
		System.out.println("de priemgetallen tot 20 zijn: ");
		println(priemgetallenTot(20));
	}

	/**
	 * Prints an array on one line.
	 * @param seq
	 */
	public static void println(int[] seq){
		String result = "";
		for (int i = 0; i < seq.length; i++){
			result += seq[i] + " ";
		}
		System.out.print(result + "\n");
	}

	/**
	 * @param seq
	 * @return the maximum value in the array
	 */
	public static int max(int[] seq){
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < seq.length; i++) {
			if (seq[i] > max) max = seq[i];
		}
		return max;
	}

	/**
	 * @param seq
	 * @param el
	 * @return the index of el in the array seq. If the element is not in the
	 * array it will return -1
	 */
	public static int index(int[] seq, int el){
		for (int i = 0; i < seq.length; i++) {
			if (seq[i] == el) return i;
		}
		return -1;
	}

	/**
	 * @param seq
	 * @param el
	 * @return true if el is in the array seq
	 */
	public static boolean bevat(int[] seq, int el){
		return index(seq, el) != -1;
	}

	/**
	 * @param n
	 * @return true if the number n is a prime number
	 */
	public static boolean isPriem(int n){
		if (n < 2) return false;
		// n is even, return true if n == 2
		if (n % 2 == 0) return (n == 2);
		// only check the first half
		int m = (int) Math.round(Math.sqrt(n));
		for (int i = 3; i <= m; i += 2){
			// n devises nicely in i, so not prime
			if (n % i == 0) return false;
		}
		return true;
	}

	/**
	 * @param seq
	 * @return the number of prime numbers in the array seq
	 */
	public static int telPriemgetallen(int[] seq){
		int n = 0;
		for (int i = 0; i < seq.length; i++){
			if (isPriem(seq[i])) n++;
		}
		return n;
	}

	/**
	 * @param seq
	 * @return all the prime numbers in the array seq
	 */
	public static int[] priemgetallenIn(int[] seq){
		int[] res = new int[seq.length];
		int n = 0;
		for (int i = 0; i < seq.length; i++){
			if (isPriem(seq[i])) res[n++] = seq[i];
		}
		return cleanUpPrimeNumbers(res, n);
	}

	/**
	 * @param n
	 * @return
	 */
	public static int[] priemgetallenTot(int max){
		if (max < 0) max = 0;
		int[] res = new int[max];
		int n = 0;
		for (int i = 0; i < max; i++){
			if (isPriem(i)) res[n++] = i;
		}
		return cleanUpPrimeNumbers(res, n);
	}

	/**
	 * Cleans up the prime numbers array so we'll get the right length
	 * @param req
	 * @param n
	 * @return
	 */
	public static int[] cleanUpPrimeNumbers(int[] req, int n){
		int[] res = new int[n];
		for (int i = 0; i < n; i++){
			res[i] = req[i];
		}
		return res;
	}

}
