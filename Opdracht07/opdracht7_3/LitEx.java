//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_3;

class LitEx extends Ex {

	double literal;

	public LitEx(double lt) {
		literal = lt;
	}

	@Override
	public double eval(double val) {
		return literal;
	}

	@Override
	public String toString() {
		return "<LitEx(" + literal + ")>";
	}

}
