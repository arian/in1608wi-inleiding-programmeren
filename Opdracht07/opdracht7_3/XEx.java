//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_3;

class XEx extends Ex {

	@Override
	public double eval(double val) {
		return val;
	}

	@Override
	public String toString() {
		return "<XEx(X)>";
	}

}
