//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_3;

import java.util.*;

abstract public class Ex {

	public abstract double eval(double val);

	/**
	 * Read the List and return the Ex object
	 * @param it
	 * @return
	 */
	protected static Ex read(Scanner sc) {

		String s = sc.next();

		if (s.equals("(")) {

			Ex operand1 = read(sc);
			char operator = sc.next().charAt(0);
			Ex operand2 = read(sc);

			// do next for closing )
			sc.next();

			return new BinEx(operand1, operator, operand2);

		} else if (s.equals("X")) {
			return new XEx();
		} else {
			return new LitEx(Double.parseDouble(s));
		}

	}

	public static Ex read(String str) {
		return read(new Scanner(str));
	}

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(read(new Scanner("( X + 3 )")).toString());
		// Some tests
		String[][] tests = {
			{"4",   "( 1 + 3 )"},
			{"5",   "( 6 + ( 2 - 3 ) )"},
			{"-10", "( ( 6 + 4 ) * ( 2 - 3 ) )"},
			{"100", "( ( 6 + ( 2 * 2 ) ) * ( 20 / 2 ) )"},
			{"100", "( ( ( 24 / 4 ) + ( 2 * 2 ) ) * ( ( 5 * 4 ) / 2 ) )"},
			{"100", "( ( ( 24 / X ) + ( 2 * 2 ) ) * ( ( 5 * X ) / 2 ) )"}
		};

		for (String[] ex : tests) {
			Ex exp = read(ex[1]);
			System.out.println(ex[1]);
			System.out.println(exp.toString());
			System.out.printf("Expects: %4s  - Equals %4.2f\n\n", ex[0], exp.eval(4));
			System.out.println(exp.eval(4));
		}

	}

}
