//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_1;

import java.util.*;

abstract public class Ex {

	public abstract String eval();

	/**
	 * Read the expressions
	 * @param sc
	 * @return
	 * @throws ExException
	 */
	protected static Ex read(Scanner sc) throws ExException {

		String c;

		c = sc.next();
		if (!c.equals("(")) {
			return new SimEx(c);
		}

		c = sc.next();
		SimEx operand1 = new SimEx(c);

		c = sc.next();
		char operator = c.charAt(0);

		c = sc.next();
		SimEx operand2 = new SimEx(c);

		c = sc.next();
		if (!c.equals(")")) {
			throw new ExException("A expression should end with a ')'");
		}

		return new BinEx(operand1, operator, operand2);
	}

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			Ex exp = read(sc);
			System.out.println(exp.toString());
			System.out.println(exp.eval());
		} catch (ExException e){
			e.printStackTrace();
		}
	}

}

class SimEx extends Ex {

	String text;

	public SimEx(String tx) {
		text = tx;
	}

	@Override
	public String eval() {
		return text;
	}

	@Override
	public String toString() {
		return "<SimEx(" + text + ")>";
	}

}

class BinEx extends Ex {

	Ex operand1, operand2;
	char operator;

	public BinEx(Ex op1, char or, Ex op2) {
		operand1 = op1;
		operator = or;
		operand2 = op2;
	}

	/**
	 * Evaluation of the expression
	 * Returns operand1 + operand2 when the operator is C
	 * Returns operand2 + operand1 when the operator is R or something else
	 * @return
	 */
	@Override
	public String eval() {
		switch (operator){
			case 'C':
				return operand1.eval() + operand2.eval();
			case 'R':
			default:
				return operand2.eval() + operand1.eval();
		}		
	}

	@Override
	public String toString() {
		return "<BinEx(" + operand1 + "," + operator + "," + operand2 + ")>";
	}

}

class ExException extends Exception {
	public ExException(String msg) {
		super(msg);
	}
}
