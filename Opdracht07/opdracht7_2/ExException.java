//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_2;

class ExException extends Exception {
	public ExException(String msg) {
		super(msg);
	}
}
