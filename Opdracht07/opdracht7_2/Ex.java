//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_2;

import java.util.*;

abstract public class Ex {

	public Ex(){

	}

	public abstract double eval();

	/**
	 * Read the List and return the Ex object
	 * @param it
	 * @return
	 */
	protected static Ex read(ListIterator it) {

		String s;
		s = (String) it.next();

		Ex operand1;
		Ex operand2;
		char operator;

		// No openening parenthesis → LitEx
		if (!s.equals("(")) {
			return new LitEx(Double.parseDouble(s));
		}

		// First operand
		s = (String) it.next();
		if (s.equals("(")) {
			// nested parenthesis, recursive read()
			// call previous to include the '('
			it.previous();
			operand1 = read(it);
		} else {
			operand1 = new LitEx(Double.parseDouble(s));
		}

		// Operator
		operator = ((String) it.next()).charAt(0);

		// Second operand
		s = (String) it.next();
		if (s.equals("(")) {
			// nested parenthesis, recursive read()
			// call previous to include the '('
			it.previous();
			operand2 = read(it);
		} else {
			operand2 = new LitEx(Double.parseDouble(s));
		}

		// call it.next() to skip the closing ')'
		s = (String) it.next();
		if (s.equals(")")) {
			// do something fancy
		}

		return new BinEx(operand1, operator, operand2);
	}

	/**
	 * Overload read() for ArrayLists
	 * @param list
	 * @return
	 */
	protected static Ex read(ArrayList<String> list) {
		return read(list.listIterator());
	}

	/**
	 * Overload read() for scanners
	 * @param sc
	 * @return
	 */
	protected static Ex read(Scanner sc) {
		ArrayList<String> list = new ArrayList<String>();
		while (sc.hasNextLine()){
			String[] line = sc.nextLine().split(" ");
			list.addAll(Arrays.asList(line));
			break;
		}
		return read(list);
	}

	/**
	 * Overload read() for strings
	 * @param expr
	 * @return
	 */
	protected static Ex read(String expr) {
		return read(Arrays.asList(expr.split(" ")).listIterator());
	}

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {

		// Some tests
		String[][] tests = {
			{"4",   "( 1 + 3 )"},
			{"5",   "( 6 + ( 2 - 3 ) )"},
			{"-10", "( ( 6 + 4 ) * ( 2 - 3 ) )"},
			{"100", "( ( 6 + ( 2 * 2 ) ) * ( 20 / 2 ) )"},
			{"100", "( ( ( 24 / 4 ) + ( 2 * 2 ) ) * ( ( 5 * 4 ) / 2 ) )"},
		};

		for (String[] ex : tests) {
			Ex exp = read(ex[1]);
			System.out.println(ex[1]);
			System.out.println(exp.toString());
			System.out.printf("Expects: %4s  - Equals %4.2f\n\n", ex[0], exp.eval());
			System.out.println(exp.eval());
		}

		Scanner sc = new Scanner(System.in);

		Ex exp = read(sc);
		System.out.println(exp.toString());
		System.out.println(exp.eval());

	}

}
