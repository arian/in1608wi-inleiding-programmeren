//	Practicum IN1608WI	Opdracht 7
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 26 10 2011

package opdracht7.opdracht7_2;

class BinEx extends Ex {

	Ex operand1, operand2;
	char operator;

	public BinEx(Ex op1, char or, Ex op2) {
		operand1 = op1;
		operator = or;
		operand2 = op2;
	}

	/**
	 * Evaluation of the expression
	 * Returns operand1 + operand2 when the operator is C
	 * Returns operand2 + operand1 when the operator is R or something else
	 * @return
	 */
	@Override
	public double eval() {
		switch (operator){
			case '+':
				return operand1.eval() + operand2.eval();
			case '-':
				return operand1.eval() - operand2.eval();
			case '*':
				return operand1.eval() * operand2.eval();
			case '/':
				return operand1.eval() / operand2.eval();
		}
		return operand1.eval();
	}

	@Override
	public String toString() {
		return "<BinEx(" + operand1 + "," + operator + "," + operand2 + ")>";
	}

}
