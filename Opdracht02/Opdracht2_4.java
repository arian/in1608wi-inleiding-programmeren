//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

/**
 * Prints a triangle of 6 by 6 stars
 * @author arian
 */
public class Opdracht2_4 {

	public static void main(String[] args) {

		for (int i = 0; i < 6; i++){
			for (int j = 6 - i; j <= 6; j++){
				System.out.print("* ");
			}
			System.out.print("\n");
		}

	}
}
