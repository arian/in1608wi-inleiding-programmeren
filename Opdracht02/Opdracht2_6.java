//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

/**
 * Prints a triangle of numbers, from 00 to 55, as yx
 * @author arian
 */
public class Opdracht2_6 {

	public static void main(String[] args) {

		for (int y = 0; y < 6; y++){
			for (int x = 0; x <= y; x++){
				System.out.print("" + y + "" + x + " ");
			}
			System.out.print("\n");
		}

	}
}
