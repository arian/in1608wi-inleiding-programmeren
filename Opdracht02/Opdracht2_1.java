//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

import java.util.*;

/**
 * Determines which of the input numbers is the biggest with
 * nested statements
 * @author arian
 */
public class Opdracht2_1 {

	public static void main(String[] args) {

		System.out.print("Geef 3 onderling verschillende gehele getallen: ");

		// input of three numbers
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		int num3 = sc.nextInt();

		String max;

		// determine which one is the biggest
		if (num1 > num2){
			max = num3 > num1 ? "derde" : "eerste";
		} else {
			max = num3 > num2 ? "derde" : "tweede";
		}

		// output
		System.out.println("\nHet " + max + " getal is het grootst");
	}
}
