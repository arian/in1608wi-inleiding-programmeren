//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

import java.util.*;

/**
 * Determines which of the input numbers is the biggest without nested statements
 * @author arian
 */
public class Opdracht2_2 {

	public static void main(String[] args) {

		System.out.print("Geef 3 onderling verschillende gehele getallen: ");

		// input of three numbers
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		int num3 = sc.nextInt();

		String max;

		// determine which one is the biggest
		if (num1 > num2 && num1 > num3){
			max = "eerste";
		} else if (num2 > num1 && num2 > num3){
			max = "tweede";
		} else {
			max = "derde";
		}

		// output
		System.out.println("\nHet " + max + " getal is het grootst");
	}
}
