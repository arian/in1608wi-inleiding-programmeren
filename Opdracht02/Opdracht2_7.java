//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

/**
 * Calculates the values of a function from a lower to a upper boundary.
 * also calculates the sum
 * @author arian
 */
public class Opdracht2_7 {

	public static void main(String[] args) {

		double lower = 0.0,
			upper = 5.0,
			step = 0.001,
			sum = 0.0;

		String x = "",
			result = "";

		// loop from lower to upper
		for (double i = lower; i <= upper; i += step){
			x += i + " ";
			double fnResult = fn(i);
			result += fnResult + " ";
			sum += fnResult * step;
		}

		System.out.print(x + "\n" + result + "\n" + sum + "\n");
	}

	/**
	 * The ultra special and complicated function which we want
	 * to calculate some values.
	 * @param i
	 * @return result of the calculation
	 */
	private static double fn(double i){
		return 2 * i + 1;
	}
}
