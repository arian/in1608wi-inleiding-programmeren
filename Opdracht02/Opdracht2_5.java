//	Practicum IN1608WI	Opdracht 2
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 21 9 2011

package opdracht2;

/**
 * Prints two triangles of 6x6 stars
 * @author arian
 */
public class Opdracht2_5 {

	public static void main(String[] args) {

		for (int i = 0; i < 6; i++){
			int j;
			// collect the stars in a string, so we don't have to loop twice
			String stars = "";
			String space = "";
			for (j = 6 - i; j <= 6; j++){
				stars += "* ";
			}
			// intermediate whitespace
			for (j = 0; j < 5 - i; j++){
				space += "  ";
			}
			System.out.print(stars + space + stars + "\n");
		}

	}
}
