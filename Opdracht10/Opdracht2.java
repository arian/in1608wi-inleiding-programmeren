//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.FileInputStream;
import java.io.IOException;

public class Opdracht2 {

	public static void main(String[] argv) {

		try {

			FileInputStream stream = new FileInputStream("/home/arian/dev/Opdracht1/src/opdracht10/data.txt");

			int ch;
			while ((ch = stream.read()) != -1) {
				System.out.print(ch + " ");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
