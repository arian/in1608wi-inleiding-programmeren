//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Opdracht7 {

	public static void main(String[] argv) {

		try {

			FileReader reader = new FileReader("/home/arian/dev/Opdracht1/src/opdracht10/student.txt");
			BufferedReader buffer = new BufferedReader(reader);

			Student student = Student.lees(buffer);

			System.out.println(student);

			FileWriter writer = new FileWriter("/home/arian/dev/Opdracht1/src/opdracht10/student1.txt");
			PrintWriter print = new PrintWriter(writer);

			student.setNaam("Ariakln");
			student.schrijf(print);

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
