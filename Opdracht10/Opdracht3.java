//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Opdracht3 {

	public static void main(String[] argv) {

		try {

			FileInputStream stream = new FileInputStream("/home/arian/dev/Opdracht1/src/opdracht10/data.txt");

			Scanner scanner = new Scanner(stream);

			int _integer = scanner.nextInt();
			double _double = scanner.nextDouble();
			String _string = scanner.next() + scanner.nextLine();

			System.out.println("De integer is: " + _integer);
			System.out.println("De double is: " + _double);
			System.out.println("De string is: " + _string);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
