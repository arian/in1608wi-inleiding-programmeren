//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Student {

	private String naam;
	private String studieNummer;
	private int leeftijd;

	public Student() {
	}

	public Student(String nm, String sr, int ld) {
		setNaam(nm);
		setStudieNummer(sr);
		setLeeftijd(ld);
	}

	public void setNaam(String nm) {
		naam = nm;
	}

	public void setStudieNummer(String sr) {
		studieNummer = sr;
	}

	public void setLeeftijd(int ld) {
		leeftijd = ld;
	}

	public String getNaam() {
		return naam;
	}

	public String getStudieNummer() {
		return studieNummer;
	}

	public int getLeeftijd() {
		return leeftijd;
	}

	@Override
	public String toString() {
		String res = "<Student(";
		res = res + naam + ",";
		res = res + studieNummer + ",";
		res = res + leeftijd + ")>";
		return res;
	}

	public static Student lees(BufferedReader br) throws IOException {
		String naam = br.readLine();
		String stdnummer = br.readLine();
		int leeftijd = Integer.parseInt(br.readLine());
		return new Student(naam, stdnummer, leeftijd);
	}

	public void schrijf (PrintWriter pw) {
		pw.println(naam);
		pw.println(studieNummer);
		pw.println("" + leeftijd);
	}

}
