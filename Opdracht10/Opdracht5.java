//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Opdracht5 {

	public static void main(String[] argv) {

		try {

			FileReader reader = new FileReader("/home/arian/dev/Opdracht1/src/opdracht10/data.txt");
			BufferedReader buffer = new BufferedReader(reader);

			int _integer = Integer.parseInt(buffer.readLine());
			double _double = Double.parseDouble(buffer.readLine());
			String _string = buffer.readLine();

			System.out.println("De integer is: " + _integer);
			System.out.println("De double is: " + _double);
			System.out.println("De string is: " + _string);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
