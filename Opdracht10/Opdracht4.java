//	Practicum IN1608WI	Opdracht 10
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 08 12 2011

package opdracht10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Opdracht4 {

	public static void main(String[] argv) {

		try {

			FileReader reader = new FileReader("/home/arian/dev/Opdracht1/src/opdracht10/data.txt");
			BufferedReader buffer = new BufferedReader(reader);

			String line;
			while ((line = buffer.readLine()) != null) {
				System.out.println(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
