//	Practicum IN1608WI	Opdracht 9
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 01 12 2011

package opdracht9;

import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JFrame;

public class TekenScherm1 extends JFrame {

	private static final long serialVersionUID = 1L;

	private final TekenPaneel tPaneel;

	public TekenScherm1(PuntenLijst pl) {
		setTitle("Tekenscherm");
		setBounds(100, 100, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		tPaneel = new TekenPaneel(pl);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		tPaneel.teken(g);
	}

	public static void main(String[] argv) {
		PuntenLijst pl = new PuntenLijst();

		pl.voegTo(new Point(100, 100));
		pl.voegTo(new Point(200, 200));
		pl.voegTo(new Point(100, 200));

		new TekenScherm1(pl);

	}

}
