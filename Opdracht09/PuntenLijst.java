//	Practicum IN1608WI	Opdracht 9
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 01 12 2011

package opdracht9;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

public class PuntenLijst {

	private final ArrayList<Point> punten;

	public PuntenLijst() {
		punten = new ArrayList<Point>();
	}

	public void voegTo(Point p) {
		if (p != null) {
			punten.add(p);
		}
	}

	public Point get(int i) {
		if (i < aantal()) {
			return punten.get(i);
		}
		return null;
	}

	public int aantal() {
		return punten.size();
	}

	@Override
	public String toString() {
		String result = "";
		for (Point point : punten) {
			result += point.toString();
		}
		return result;
	}

	public void teken(Graphics g) {
		for (int i = 0, l = punten.size(); i < l; i++) {
			Point p1;
			Point p2 = punten.get(i);
			if (i > 0) {
				p1 = punten.get(i - 1);
			} else {
				p1 = punten.get(l - 1);
			}
			g.drawLine(p1.x, p1.y, p2.x, p2.y);
		}
	}

}
