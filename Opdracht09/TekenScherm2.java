//	Practicum IN1608WI	Opdracht 9
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 01 12 2011

package opdracht9;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TekenScherm2 extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final PuntenLijst pLijst = new PuntenLijst();

	private final JLabel xLabel;
	private final JLabel yLabel;
	private final JTextField xVeld;
	private final JTextField yVeld;
	private final JButton invoerKnop;
	private final JButton tekenKnop;
	private final JPanel paneel;
	private final TekenPaneel tPaneel;

	public TekenScherm2() {
		setTitle("Tekenscherm");
		setBounds(100, 100, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new BorderLayout());

		// TekenPaneel
		pLijst.voegTo(new Point(100, 100));
		pLijst.voegTo(new Point(200, 200));
		pLijst.voegTo(new Point(100, 200));
		tPaneel = new TekenPaneel(pLijst);

		// Buttons and jazz

		xLabel = new JLabel("X-coordinaat");
		yLabel = new JLabel("Y-coordinaat");
		xVeld = new JTextField();
		yVeld = new JTextField();
		invoerKnop = new JButton("Voer in");
		tekenKnop = new JButton("Teken");

		paneel = new JPanel(new GridLayout(3, 2));
		paneel.add(xLabel);
		paneel.add(xVeld);
		paneel.add(yLabel);
		paneel.add(yVeld);
		paneel.add(invoerKnop);
		paneel.add(tekenKnop);

		// add the the parent panel
		this.add("North", tPaneel);
		this.add("South", paneel);

		// add the event listener
		invoerKnop.addActionListener(this);
		tekenKnop.addActionListener(this);

		this.setVisible(true);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		tPaneel.teken(g);
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getSource().equals(invoerKnop)) {

			pLijst.voegTo(new Point(
					Integer.parseInt(xVeld.getText()),
					Integer.parseInt(yVeld.getText())));
			xVeld.setText("");
			yVeld.setText("");

		} else if (event.getSource().equals(tekenKnop)){
			this.repaint();
		}

	}

	public static void main(String[] argv) {
		new TekenScherm2();
	}


}
