//	Practicum IN1608WI	Opdracht 9
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 01 12 2011

package opdracht9;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class TekenScherm3 extends JFrame implements MouseListener {

	private static final long serialVersionUID = 1L;

	private final PuntenLijst pLijst = new PuntenLijst();
	private final TekenPaneel tPaneel;

	public TekenScherm3() {
		setTitle("Tekenscherm");
		setBounds(100, 100, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		tPaneel = new TekenPaneel(pLijst);
		this.add(tPaneel);

		this.addMouseListener(this);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		tPaneel.teken(g);
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		pLijst.voegTo(new Point(event.getX(), event.getY()));
		this.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	public static void main(String[] argv) {
		new TekenScherm3();
	}

}
