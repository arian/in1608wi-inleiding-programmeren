//	Practicum IN1608WI	Opdracht 9
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 01 12 2011

package opdracht9;

import java.awt.Graphics;

import javax.swing.JPanel;

public class TekenPaneel extends JPanel {

	private static final long serialVersionUID = 1L;
	private final PuntenLijst pLijst;

	public TekenPaneel(PuntenLijst pl) {
		pLijst = pl;
	}

	public void teken(Graphics g) {
		pLijst.teken(g);
	}

}
