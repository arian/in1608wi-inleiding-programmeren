//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class TestBinTree {

	public static void main(String[] args) {

		System.out.println("Er is een lege BinTree gecreeerd");
		BinTree tree = new BinTree();
		System.out.println("De inhoud van de BinTree is: " + tree);
		System.out.println("Het aantal elementen van de BinTree is: " + tree.size());
		System.out.println("Het resultaat van find(12) is: " + tree.find(12));
		System.out.println("Het resultaat van find(100) is: " + tree.find(100));
		System.out.println("De som van de elementen in de BinTree is: " + tree.sum());
		System.out.println("Het grootste waarde in de BinTree is: " + tree.max());

		System.out.println("\nAan de tree zijn de waarden 12, 4 en 7 toegevoegd");

		tree.add(12);
		tree.add(4);
		tree.add(7);

		System.out.println("De inhoud van de BinTree is: " + tree);
		System.out.println("Het aantal elementen van de BinTree is: " + tree.size());
		System.out.println("Het resultaat van find(12) is: " + tree.find(12));
		System.out.println("Het resultaat van find(100) is: " + tree.find(100));
		System.out.println("De som van de elementen in de BinTree is: " + tree.sum());
		System.out.println("Het grootste waarde in de BinTree is: " + tree.max());

	}

}
