//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class BinNode {

	private int value;
	private BinNode left;
	private BinNode right;

	public BinNode(BinNode l, int v, BinNode r) {
		value = v;
		left = l;
		right = r;
	}

	public void setValue(int v) {
		value = v;
	}

	public void setLeft(BinNode l) {
		left = l;
	}

	public void setRight(BinNode r) {
		right = r;
	}

	public int getValue() {
		return value;
	}

	public BinNode getLeft() {
		return left;
	}

	public BinNode getRight() {
		return right;
	}

	@Override
	public String toString() {
		return "<BinNode(" + value + ")>";
	}

}
