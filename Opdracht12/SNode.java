//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class SNode {

	private String value;
	private SNode next;

	public SNode(String v, SNode n) {
		value = v;
		next = n;
	}

	public void setValue(String v) {
		value = v;
	}

	public void setNext(SNode n) {
		next = n;
	}

	public String getValue() {
		return value;
	}

	public SNode getNext() {
		return next;
	}

	@Override
	public String toString() {
		return "<SNode(" + value + ")>";
	}

}
