//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class SList {

	private SNode head;

	public SList() {
	}

	/**
	 * Get the number of nodes in this list
	 * @return
	 */
	public int size() {
		return size(head);
	}

	public int size(SNode r) {
		if (r == null) {
			return 0;
		}
		return 1 + size(r.getNext());
	}

	/**
	 * Add a new item to this list, alphabetically
	 * @param s
	 */
	public void addSorted(String s) {
		if (head == null || s.compareTo(head.getValue()) < 0) {
			// the very first first string in the list or the new string should
			// be the new head because s = "a" and head.getValue() = "b"
			head = new SNode(s, head);
		} else {
			addSorted(s, head);
		}
	}

	public void addSorted(String s, SNode r) {
		if (r != null) {
			SNode next = r.getNext();
			if (next == null || s.compareTo(next.getValue()) < 0) {
				// if next == null, the new SNode will be the last element in
				// the list.
				// s < the next value, so place the new node between r and next.
				r.setNext(new SNode(s, next));
			} else {
				addSorted(s, r.getNext());
			}
		}
	}

	/**
	 * Remove all strings that matches the argument "s" from the list.
	 * @param s
	 */
	public void remove(String s) {
		if (head != null && s != null) {
			if (s.equals(head.getValue())) {
				// head is already a match, so set the second item as first item
				// or to null when there isn't a second item.
				head = head.getNext();
			} else {
				remove(s, head);
			}
		}
	}

	public void remove(String s, SNode r) {
		if (r != null) {
			SNode next = r.getNext();
			if (next != null) {
				// next matches, so the r.next will become the next of the next.
				if (s.equals(next.getValue())) {
					r.setNext(next.getNext());
				}
				// although we might have a match, there might be other nodes
				// with the same value, so keep searching
				remove(s, next);
			}
		}
	}

	/**
	 * Find a SNode in this list
	 * @param s
	 * @return
	 */
	public SNode find(String s) {
		return find(s, head);
	}

	public SNode find(String s, SNode r) {
		if (r == null) {
			return null;
		}
		if (s != null && s.equals(r.getValue())) {
			return r;
		}
		return find(s, r.getNext());
	}

	/**
	 * Checks if a string is in this list
	 * @param s
	 * @return
	 */
	public boolean contains(String s) {
		return find(s) != null;
	}

	@Override
	public String toString() {
		return toString(head);
	}

	public String toString(SNode r) {
		if (r == null) {
			return "";
		}
		return r.getValue() + " " + toString(r.getNext());
	}

}
