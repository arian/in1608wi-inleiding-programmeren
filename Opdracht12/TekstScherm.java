//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TekstScherm extends JFrame implements ActionListener {

	private static final long serialVersionUID = -75711388345810101L;
	private final JButton voegtoeKnop = new JButton("VoegToe");
	private final JButton verwijderKnop = new JButton("Verwijder");

	private final JLabel woordLabel = new JLabel("Woord:");
	private final JLabel lijstLabel = new JLabel();

	private final JTextField woordVeld = new JTextField();
	private final JPanel panel = new JPanel(new GridLayout(2, 2));
	private final SList list = new SList();

	public TekstScherm() {
		setTitle("TekstScherm");
		setBounds(100, 100, 400, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		setLayout(null);

		panel.setBounds(40, 20, 300, 100);
		panel.add(voegtoeKnop);
		panel.add(verwijderKnop);
		panel.add(woordLabel);
		panel.add(woordVeld);
		c.add(panel);

		lijstLabel.setBounds(40, 120, 300, 50);
		c.add(lijstLabel);

		voegtoeKnop.addActionListener(this);
		verwijderKnop.addActionListener(this);

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == voegtoeKnop) {
			list.addSorted(woordVeld.getText());
		} else if (source == verwijderKnop) {
			list.remove(woordVeld.getText());
		}
		lijstLabel.setText(list.toString());
		woordVeld.setText("");
	}

	public static void main(String[] args) {
		new TekstScherm();
	}

}
