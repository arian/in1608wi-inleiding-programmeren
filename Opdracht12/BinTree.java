//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class BinTree {

	private BinNode root;

	public BinTree(){
	}

	public int size() {
		return size(root);
	}

	public int size(BinNode t) {
		if (t == null) {
			return 0;
		}
		return 1 + size(t.getLeft()) + size(t.getRight());
	}

	public void add(int v) {
		if (root == null) {
			root = new BinNode(null, v, null);
		} else {
			add(v, root);
		}
	}

	public void add(int v, BinNode t) {
		if (t != null) {
			if (v < t.getValue()) {
				// Smaller values on the left branch
				if (t.getLeft() == null) {
					t.setLeft(new BinNode(null, v, null));
				} else {
					add(v, t.getLeft());
				}
			} else {
				// Bigger and equal values on the right branch
				if (t.getRight() == null) {
					t.setRight(new BinNode(null, v, null));
				} else {
					add(v, t.getRight());
				}
			}
		}
	}

	public BinNode find(int v) {
		return find(v, root);
	}

	public BinNode find(int v, BinNode t) {
		if (t != null) {
			int value = t.getValue();
			if (value == v) {
				return t;
			} else if (value < v) {
				return find(v, t.getLeft());
			} else {
				return find(v, t.getRight());
			}
		}
		return null;
	}

	public int sum() {
		return sum(root);
	}

	public int sum(BinNode t) {
		if (t == null) {
			return 0;
		}
		return t.getValue() + sum(t.getLeft()) + sum(t.getRight());
	}

	public int max() {
		if (root == null) {
			// no nodes in the tree yet.
			return Integer.MIN_VALUE;
		}
		return max(root);
	}

	public int max(BinNode t) {
		BinNode right = t.getRight();
		if (right != null) {
			return max(right);
		}
		return t.getValue();
	}

	@Override
	public String toString() {
		return toString(root);
	}

	public String toString(BinNode t) {
		if (t == null) {
			return "()";
		}
		return "(" + toString(t.getLeft()) + "," + t.getValue() + "," + toString(t.getRight()) + ")";
	}

}
