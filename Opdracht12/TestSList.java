//	Practicum IN1608WI	Opdracht 12
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 22 12 2011

package opdracht12;

public class TestSList {

	public static void main(String[] args) {

		System.out.println("Er is een lege SLijst gecreeerd");
		SList list = new SList();

		System.out.println("De inhoud van SLijst is: " + list);
		System.out.println("Het aantal elementen van SLijst is: " + list.size());
		System.out.println("Het resultaat van find(\"aap\") is: " + list.find("aap"));
		System.out.println("Het resultaat van find(\"vuur\") is: " + list.find("vuur"));

		list.addSorted("aap");
		list.addSorted("noot");
		list.addSorted("mies");

		System.out.println("Aan de lijst zijn 'aap', 'noot' en 'mies' toegevoegd");

		System.out.println("De inhoud van SLijst is: " + list);
		System.out.println("Het aantal elementen van SLijst is: " + list.size());
		System.out.println("Het resultaat van find(\"aap\") is: " + list.find("aap"));
		System.out.println("Het resultaat van find(\"vuur\") is: " + list.find("vuur"));

		list.remove("aap");
//		list.addSorted("mies");
		list.remove("vuur");

		System.out.println("Aan de lijst zijn 'aap', 'vuur' verwijderd");

		System.out.println("De inhoud van SLijst is: " + list);
		System.out.println("Het aantal elementen van SLijst is: " + list.size());
		System.out.println("Het resultaat van find(\"aap\") is: " + list.find("aap"));
		System.out.println("Het resultaat van find(\"vuur\") is: " + list.find("vuur"));


//		list.addSorted("yoo");
//		list.addSorted("g");
//		list.addSorted("g0");
//		list.addSorted("arian");
//		list.addSorted("baa");
//		list.addSorted("bba");
//
//		System.out.println(list.toString());
//
//		System.out.println(list.find("g").toString());
//
//		list.remove("g");
//
//		System.out.println(list.toString());


	}

}
