/**
 * @author: Arian stolwijk
 * @studienummer: 4001079
 */
package opdracht1;

public class Opdracht1_4 {

	public static void main(String[] args) {

		int getal1, getal2, getal3, temp;

		getal1 = 2;
		getal2 = 3;
		getal3 = 5;

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2);
		System.out.println("De waarde van getal3 is: " + getal3 + '\n');

		temp = getal3;
		getal3 = getal2;
		getal2 = getal1;
		getal1 = temp;

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2);
		System.out.println("De waarde van getal3 is: " + getal3 + '\n');

	}
}
