/**
 * @author: Arian stolwijk
 * @studienummer: 4001079
 */
package opdracht1;

public class Opdracht1_2 {

	public static void main(String[] args) {

		int getal = 10;

		System.out.println("Een klein getal: " + getal);

		getal++;
		
		System.out.println("Een klein getal plus een: " + getal);

		getal = Integer.MAX_VALUE;

		System.out.println("\nEen heel groot getal: " + getal);

		getal++;
		
		System.out.println("Een heel groot getal plus een: " + getal);

	}
}
