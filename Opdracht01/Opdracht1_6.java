/**
 * @author: Arian stolwijk
 * @studienummer: 4001079
 */
package opdracht1;

import java.util.*;

public class Opdracht1_6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		double temperature1Celsius, temperature1Fahrenheit;
		double temperature2Fahrenheit, temperature2Celsius;

		System.out.println("Voer 2 temperaturen in, in graden Celsius en in Fahrenheit:");

		temperature1Celsius = sc.nextDouble();
		temperature2Fahrenheit = sc.nextDouble();

		System.out.println("\nDe waarde van de eerste temperatuur is " + temperature1Celsius + " graden Celsius");
		System.out.println("De waarde van de tweede temperatuur is " + temperature2Fahrenheit + " graden Fahrenheit");

		temperature1Fahrenheit = CelsiusToFahrenheit(temperature1Celsius);
		temperature2Celsius = FahrenheitToCelsius(temperature2Fahrenheit);

		System.out.println("\nDe waarde van de eerste temperatuur is " + temperature1Fahrenheit + " graden Fahrenheit");
		System.out.println("De waarde van de tweede temperatuur is " + temperature2Celsius + " graden Celsius");

	}

	private static double CelsiusToFahrenheit(double c){
		return c * 9 / 5 + 32.0;
	}

	private static double FahrenheitToCelsius(double f){
		return (f - 32.0) * 5 / 9;
	}

}
