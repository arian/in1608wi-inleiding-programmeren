/**
 * @author: Arian stolwijk
 * @studienummer: 4001079
 */
package opdracht1;

public class Opdracht1_3 {

	public static void main(String[] args) {

		int getal1, getal2, temp;

		getal1 = 5;
		getal2 = 10;

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2 + '\n');

		temp = getal1;
		getal1 = getal2;
		getal2 = temp;

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2);

	}
}
