/**
 * @author: Arian stolwijk
 * @studienummer: 4001079
 */
package opdracht1;

import java.util.*;

public class Opdracht1_5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int getal1, getal2, temp;

		System.out.println("Voer de waarde van variabelen getal1 en getal2 in:");

		getal1 = sc.nextInt();
		getal2 = sc.nextInt();

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2 + '\n');

		temp = getal1;
		getal1 = getal2;
		getal2 = temp;

		System.out.println("De waarde van getal1 is: " + getal1);
		System.out.println("De waarde van getal2 is: " + getal2);

	}
}
