//	Practicum IN1608WI	Opdracht 6
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht6;

public class BitSet {

	private boolean[] elements;
	private int aantal;

	/**
	 * Creates a new BitSet object with capacity n
	 * @param n
	 */
	public BitSet(int n) {
		if (n < 0) n = 0;
		this.elements = new boolean[n];
		this.aantal = 0;
	}

	/**
	 * Add a new element if it fits into the list capacity
	 * @param el
	 */
	public void add(int el){
		if (el >= 0 && el < elements.length){
			elements[el] = true;
			aantal++;
		}
	}

	/**
	 * @param el
	 * @return true if the element is in the bitset
	 */
	public boolean contains(int el){
		return el >= 0 
			&& el < elements.length
			&& elements[el] == true;
	}

	/**
	 * @return the current size of the list
	 */
	public int getSize(){
		return aantal;
	}

	/**
	 * returns a new bitset where each item in the elements is true if those
	 * elements in this element OR in that element are true
	 * @param that
	 * @return
	 */
	public BitSet vereniging(BitSet that){
		int n = Math.min(elements.length, that.elements.length);
		BitSet set = new BitSet(n);
		for (int i = 0; i < n; i++){
			if (elements[i] || that.elements[i]){
				set.add(i);
			}
		}
		return set;
	}

	/**
	 * Returns a new BitSet where each item in the elements is true if those
	 * elements in this element AND in that element are true
	 * @param that
	 * @return
	 */
	public BitSet doorsnijding(BitSet that){
		int n = Math.min(elements.length, that.elements.length);
		BitSet set = new BitSet(n);
		for (int i = 0; i < n; i++){
			if (elements[i] && that.elements[i]){
				set.add(i);
			}
		}
		return set;
	}

	/**
	 * Returns a new BitSet with elements that are in this but are not in that
	 * @param that
	 * @return
	 */
	public BitSet verschil(BitSet that){
		int n = Math.min(elements.length, that.elements.length);
		BitSet set = new BitSet(n);
		for (int i = 0; i < n; i++){
			if (elements[i] && !that.elements[i]){
				set.add(i);
			}
		}
		return set;
	}

	/**
	 * @param other
	 * @return true if the lists are equal
	 */
	@Override
	public boolean equals(Object other){
		if (other instanceof BitSet){
			BitSet s = (BitSet) other;
			if (aantal == s.aantal){
				for (int i = 0; i < aantal; i++){
					if (elements[i] != s.elements[i]){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * @return a string representation of the list
	 */
	public String toString(){
		String res = "<BitSet[";
		int n = 0;
		for (int i = 0; i < elements.length; i++){
			if (elements[i]){
				if (n++ > 0) res += ",";
				res += i;
			}
		}
		return res + "]>";
	}


}
