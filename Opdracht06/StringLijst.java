//	Practicum IN1608WI	Opdracht 6
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht6;

public class StringLijst {

	private String[] elements;
	private int aantal;

	/**
	 * Creates a new StringLijst object with capacity n
	 * @param n
	 */
	public StringLijst(int n) {
		if (n < 0) n = 0;
		this.elements = new String[n];
		this.aantal = 0;
	}

	/**
	 * Add a new element if it fits into the list capacity
	 * @param el
	 */
	public void add(String el){
		if (aantal < elements.length){
			elements[aantal++] = el;
		}
	}

	/**
	 * Gets a item from the list with index i
	 * @param i
	 * @return
	 */
	public String get(int i){
		if (i >= 0 && i < aantal) {
			return elements[i];
		}
		return null;
	}

	/**
	 * Sets a item to the list 
	 * @param i
	 * @param el
	 */
	public void set(int i, String el){
		if (i >= 0 && i < aantal) {
			elements[i] = el;
		}
	}

	/**
	 * @param el
	 * @return the index of `el` in the list, otherwise it will return `null`
	 */
	public int index(String el){
		for (int i = 0; i < aantal; i++){
			if (elements[i].equals(el)) return i;
		}
		return -1;
	}

	/**
	 * @param el
	 * @return true if the el is in the list, otherwise false
	 */
	public boolean contains(String el){
		return index(el) != -1;
	}

	/**
	 * @return the current size of the list
	 */
	public int getSize(){
		return aantal;
	}

	/**
	 * @param other
	 * @return true if the lists are equal
	 */
	@Override
	public boolean equals(Object other){
		if (other instanceof StringLijst){
			StringLijst l = (StringLijst) other;
			if (aantal == l.aantal){
				for (int i = 0; i < aantal; i++){
					if (!elements[i].equals(l.elements[i])){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * @return a string representation of the list
	 */
	public String toString(){
		String res = "<StringLijst[";
		for (int i = 0; i < aantal; i++){
			if (i > 0) res += ",";
			res += elements[i];
		}
		return res + "]>";
	}

}
