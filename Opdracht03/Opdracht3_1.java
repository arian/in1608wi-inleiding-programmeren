//	Practicum IN1608WI	Opdracht 3
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 28 9 2011

package opdracht3;

public class Opdracht3_1 {

	public static void main(String[] args) {

		System.out.println("    1: " + integraal(0, 5, 1));
		System.out.println("  0.1: " + integraal(0, 5, 0.1));
		System.out.println(" 0.01: " + integraal(0, 5, 0.01));
		System.out.println("0.001: " + integraal(0, 5, 0.001));

		testOpdracht3_1();

	}

	/**
	 * A function
	 * @param x
	 * @return double
	 */
	public static double f(double x){
		return x;
	}

	/**
	 * Integrates the function "f" from lower to upper
	 * @param lower lower boundary
	 * @param upper upper boundary
	 * @param step step size
	 * @return the calculated integral of "f"
	 */
	public static double integraal(double lower, double upper, double step){
		double result = 0;
		for (double i = lower; i <= upper; i += step){
			result += f(i) * step;
		}
		return result;
	}

	public static void testOpdracht3_1(){
		System.out.println("f(3.0)  ==  3.0 is " + (Math.abs(f(3.0) - 3.0) <= 1E-9));
 		System.out.println("f(-3.0) == -3.0 is " + (Math.abs(f(-3.0) + 3.0) <= 1E-9));
 		System.out.println("integraal(0,50,1)     == 1275.0000000000 is " + (Math.abs(integraal(0,50,1) - 1275.0000000000) <= 1E-9));
 		System.out.println("integraal(0,50,0.1)   == 1247.5000000000 is " + (Math.abs(integraal(0,50,0.1) - 1247.5000000000) <= 1E-9));
		System.out.println("integraal(0,50,0.01)  == 1250.2500000000 is " + (Math.abs(integraal(0,50,0.01) - 1250.2500000000) <= 1E-9));
   		System.out.println("integraal(0,50,0.001) == 1250.0250000000 is " + (Math.abs(integraal(0,50,0.001) - 1250.0250000000) <= 1E-9));
	}

}
