//	Practicum IN1608WI	Opdracht 11
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 15 12 2011

package opdracht11;

import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

public class RecursivePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public enum Version {
		TEKEN1, TEKEN2, TEKEN3, TEKEN4
	};
	private Version version = Version.TEKEN1;

	private int recursion = 6;

	public RecursivePanel() {
	}

	public RecursivePanel(Version version, int recursion) {
		this.version = version;
		this.recursion = recursion;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Point p1 = new Point(50, 50);
		Point p2 = new Point(350, 50);
		Point p3 = new Point(200, 350);

		switch (version) {
			case TEKEN1: teken1(p1, p2, p3, recursion, g); break;
			case TEKEN2: teken2(p1, p2, p3, recursion, g); break;
			case TEKEN3: teken3(p1, p2, p3, recursion, g); break;
			case TEKEN4: teken4(p1, p2, p3, recursion, g); break;
		}

	}

	public void teken1(Point p1, Point p2, Point p3, int n, Graphics g) {
		if (n > 0) {

			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p2.x, p2.y, p3.x, p3.y);
			g.drawLine(p3.x, p3.y, p1.x, p1.y);

			Point q = midden(p1, p2);

			teken1(p1, p3, q, n - 1, g);
			teken1(p2, p3, q, n - 1, g);
		}
	}

	private void teken2(Point p1, Point p2, Point p3, int n, Graphics g) {
		if (n > 0) {

			Point q = midden(p1, p2, p3);

			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p2.x, p2.y, p3.x, p3.y);
			g.drawLine(p3.x, p3.y, p1.x, p1.y);

			teken2(p1, p2, q, n - 1, g);
			teken2(p2, p3, q, n - 1, g);
			teken2(p3, p1, q, n - 1, g);
		}
	}

	private void teken3(Point p1, Point p2, Point p3, int n, Graphics g) {
		if (n > 0) {

			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p2.x, p2.y, p3.x, p3.y);
			g.drawLine(p3.x, p3.y, p1.x, p1.y);

			Point q12 = midden(p1, p2);
			Point q23 = midden(p2, p3);
			Point q31 = midden(p3, p1);

			teken3(p1, q12, q31, n - 1, g);
			teken3(p2, q12, q23, n - 1, g);
			teken3(p3, q31, q23, n - 1, g);
		}
	}

	private void teken4(Point p1, Point p2, Point p3, int n, Graphics g) {
		if (n > 0) {

			Point q12 = midden(p1, p2);
			Point q23 = midden(p2, p3);
			Point q31 = midden(p3, p1);

			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p2.x, p2.y, p3.x, p3.y);
			g.drawLine(p3.x, p3.y, p1.x, p1.y);

			teken4(p1, q12, q31, n - 1, g);
			teken4(q12, p2, q23, n - 1, g);
			teken4(q23, q31, q12,n - 1, g);

		}
	}

	public Point midden(Point p1, Point p2) {
		return new Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
	}

	public Point midden(Point p1, Point p2, Point p3) {
		return new Point((p1.x + p2.x + p3.x) / 3, (p1.y + p2.y + p3.y) / 3);
	}

}
