//	Practicum IN1608WI	Opdracht 11
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 15 12 2011

package opdracht11;


public class Binomiaal {

	private static int aantal;
	private static int[][] array = new int[10][10];

	public static int Bin(int n, int k) {

		aantal++;

		int result = 0;

		if ((n == k && k > 0) || (n > k && k == 0)) {
			result = 1;
		} else if (n > k && k > 0){
			result = Bin(n - 1, k) + Bin(n - 1, k - 1);
		}

		return result;
	}

	public static int Bin2(int n, int k) {
		aantal++;

		if (array[n][k] != 0) {
			return array[n][k];
		}

		int result = 0;

		if ((n == k && k > 0) || (n > k && k == 0)) {
			result = 1;
		} else if (n > k && k > 0){
			result = Bin2(n - 1, k) + Bin2(n - 1, k - 1);
		}

		array[n][k] = result;
		return result;
	}

	public static void main(String[] argv) {

		aantal = 0;
		System.out.println(Bin(4,2) + "   " + aantal);
		aantal = 0;
		System.out.println(Bin(6,2) + "   " + aantal);
		aantal = 0;
		System.out.println(Bin(8,2) + "   " + aantal);
		aantal = 0;
		System.out.println(Bin(4,4) + "   " + aantal);
		aantal = 0;
		System.out.println(Bin(6,4) + "   " + aantal);
		aantal = 0;
		System.out.println(Bin(8,4) + "   " + aantal);


		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(4,2) + "   " + aantal);
		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(6,2) + "   " + aantal);
		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(8,2) + "   " + aantal);
		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(4,4) + "   " + aantal);
		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(6,4) + "   " + aantal);
		aantal = 0;
		array = new int[10][10];
		System.out.println(Bin2(8,4) + "   " + aantal);

	}

}
