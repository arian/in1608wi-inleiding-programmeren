//	Practicum IN1608WI	Opdracht 11
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 15 12 2011

package opdracht11;

import java.awt.Container;

import javax.swing.JFrame;

public class TekenDriehoeken {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Driehoeken");
		frame.setBounds(100, 100, 400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		RecursivePanel recPanel = new RecursivePanel(RecursivePanel.Version.TEKEN4, 6);
		Container container = frame.getContentPane();
		container.add(recPanel);
		frame.setVisible(true);
	}

}
