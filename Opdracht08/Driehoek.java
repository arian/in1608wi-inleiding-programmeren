//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Color;
import java.awt.Graphics;

public class Driehoek extends MKVorm {

	private Punt p1, p2, p3;

	/**
	 * Constructs the driekhoek object with three Punt objects
	 * @param p1
	 * @param p2
	 * @param p3
	 */
	public Driehoek(Punt p1, Punt p2, Punt p3){
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.green);
		int[] xs = {(int) p1.getX(), (int) p2.getX(), (int) p3.getX()};
		int[] ys = {(int) p1.getY(), (int) p2.getY(), (int) p3.getY()};
		g.drawPolygon(xs, ys, 3);
	}

	/**
	 * Gets the first Punt object
	 */
	public Punt getPunt1() {
		return p1;
	}

	/**
	 * Gets the second Punt object
	 */
	public Punt getPunt2() {
		return p2;
	}

	/**
	 * Gets the third Punt object
	 */
	public Punt getPunt3() {
		return p3;
	}

	/**
	 * Gets the circumference of the triangle
	 */
	@Override
	public double omtrek(){
		return p1.afstand(p2)
			+ p2.afstand(p3)
			+ p3.afstand(p1);
	}

	/**
	 * Gets the surface of the triangle
	 */
	public double oppervlakte(){
		double a = p1.afstand(p2);
		double b = p2.afstand(p3);
		double c = p3.afstand(p1);
		double s = (a + b + c) / 2;
		return Math.sqrt(s * (s - a) * (s - b) * (s -c));
	}

	/**
	 * Translates each point of the triangle with dx and dy
	 * @param dx
	 * @param dy
	 */
	@Override
	public void transleer(double dx, double dy){
		p1.transleer(dx, dy);
		p2.transleer(dx, dy);
		p3.transleer(dx, dy);
	}

	/**
	 * Checks if the other object equals this object
	 * @param other
	 */
	@Override
	public boolean equals(Object other){
		if (other instanceof Driehoek){
			Driehoek d = (Driehoek) other;
			return p1.equals(d.p1)
				&& p2.equals(d.p2)
				&& p3.equals(d.p3);
		}
		return false;
	}

	/**
	 * String representation of this object
	 */
	@Override
	public String toString(){
		return "<Driehoek(" + p1 + "," + p2 + "," + p3 + ")>";
	}

}
