//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Graphics;
import java.util.ArrayList;

public class MKVormLijst {

	private ArrayList<MKVorm> vorm;

	public MKVormLijst() {
		vorm = new ArrayList<MKVorm>();
	}

	public int aantal() {
		return vorm.size();
	}

	public void voegToe(MKVorm mv) {
		vorm.add(mv);
	}

	public MKVorm get(int i) {
		// http://download.oracle.com/javase/6/docs/api/java/util/ArrayList.html#get%28int%29
		// vorm.get werpt al een IndexOutOfBoundsException
		return vorm.get(i);
	}

	public int indexof(MKVorm mv) {
		return vorm.indexOf(mv);
	}

	@Override
	public String toString() {
		String ret = "<MKVormLijst[";
		for (MKVorm mv : vorm) {
			ret += mv.toString();
		}
		return ret + "]";
	}

	public void paint(Graphics g) {
		for (MKVorm mv : vorm) {
			mv.paint(g);
		}
	}

}
