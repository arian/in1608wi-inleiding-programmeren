//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Color;
import java.awt.Graphics;

public class Cirkel extends MKVorm{

	private Punt middelpunt;
	private double straal;

	/**
	 * Creates a circle
	 * @param mp
	 * @param st
	 */
	Cirkel(Punt mp, double st){
		middelpunt = mp;
		if (st < 0) straal = -1;
		else straal = st;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.blue);
		g.drawOval(
			(int) middelpunt.getX(),
			(int) middelpunt.getY(),
			(int) (straal * 2), (int) (straal * 2) );
	}

	/**
	 * Gets the middle point
	 */
	public Punt getMiddelpunt(){
		return middelpunt;
	}

	/**
	 * Gets the radius
	 */
	public double getStraal(){
		return straal;
	}

	/**
	 * Gets the circumference
	 */
	@Override
	public double omtrek(){
		return straal * 2 * Math.PI;
	}

	/**
	 * Gets the surface
	 */
	public double oppervlakte(){
		return straal * straal * Math.PI;
	}

	/**
	 * translates the middle point circle
	 * @param dx
	 * @param dy
	 */
	@Override
	public void transleer(double dx, double dy){
		middelpunt.transleer(dx, dx);
	}

	/**
	 * Checks if there is any overlap with another circle
	 * @param that
	 */
	public boolean overlapt(Cirkel that){
		double afstand = that.getMiddelpunt().afstand(middelpunt);
		return afstand <= (that.straal + this.straal);
	}

	/**
	 * Checks if the other object equals this object
	 * @param other
	 */
	@Override
	public boolean equals(Object other){
		if (other instanceof Cirkel){
			Cirkel c = (Cirkel) other;
			return this.middelpunt.equals(c.middelpunt) && c.straal == this.straal;
		}
		return false;
	}

	/**
	 * String representation of this object
	 */
	@Override
	public String toString(){
		return "<Cirkel(" + middelpunt + "," + straal + ")>";
	}

}
