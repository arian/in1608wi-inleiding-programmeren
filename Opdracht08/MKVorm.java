//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Graphics;

public abstract class MKVorm {

	public abstract void transleer(double dx, double dy);

	public abstract double omtrek();

	public abstract void paint(Graphics g);

}
