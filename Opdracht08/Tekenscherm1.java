//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Graphics;
import javax.swing.JFrame;

public class Tekenscherm1 extends JFrame {

	MKVormLijst mLijst;

	public Tekenscherm1(MKVormLijst ml) {

		mLijst = ml;

		setTitle("Tekenscherm");
		setBounds(100, 100, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		mLijst.paint(g);
	}

}
