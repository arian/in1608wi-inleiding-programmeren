/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht8;

/**
 *
 * @author arian
 */
public class TestMKVormLijst {

	public static void main (String[] argv) {

		Cirkel ck = new Cirkel(new Punt(100, 100), 100);
		Driehoek dr = new Driehoek(
			new Punt(100, 100),
			new Punt(200, 100),
			new Punt(100, 200) );

		MKVormLijst mLijst = new MKVormLijst();

		System.out.println("Cirkel ck: " + ck);
		System.out.println("Driehoek dr: " + dr);
		System.out.println("MKVormLijst mLijst: " + mLijst);

		mLijst.voegToe(dr);

		System.out.println("\nMKVormLijst mLijst: " + mLijst);

		System.out.println("mLijst.indexOf(" + ck + "): " + mLijst.indexof(ck));
		System.out.println("mLijst.indexOf(" + dr + "): " + mLijst.indexof(dr));

		System.out.println("mLijst.get(0): " + mLijst.get(0));
		System.out.println("mLijst.get(3): " + mLijst.get(3));

	}

}
