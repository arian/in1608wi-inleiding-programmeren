/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht8;

/**
 *
 * @author arian
 */
public class TestMKVorm {

	public static void main(String[] argv) {

		MKVorm mv;

		Driehoek dr = new Driehoek(
			new Punt(100, 100),
			new Punt(200, 100),
			new Punt(100, 200) );

		System.out.println("dr.toString() : " + dr);


		dr.transleer(100, 100);
		System.out.println("dr.transleer(100,100)");

		System.out.println("mv = dr");
		mv = dr;

		System.out.println("mv.toString() : " + mv);

		mv.transleer(100, 100);
		System.out.println("mv.transleer(100,100)");

		System.out.println("mv.toString() : " + mv);



		System.out.println("\n");

		Cirkel ck = new Cirkel(new Punt(300, 300), 50);

		System.out.println("ck.toString() : " + ck);

		ck.transleer(-100, -100);
		System.out.println("ck.transleer(100,100)");

		System.out.println("ck.toString() : " + ck);

		System.out.println("mv = ck");
		mv = ck;

		System.out.println("mv.toString() : " + mv);

		mv.transleer(-100, -100);
		System.out.println("mv.transleer(100,100)");

		System.out.println("mv.toString() : " + mv);

	}

}
