//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import java.awt.Graphics;
import javax.swing.JFrame;

public class Tekenscherm extends JFrame {

	public Tekenscherm() {

		setTitle("Tekenscherm");
		setBounds(100, 100, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Cirkel circ = new Cirkel(new Punt(40, 40), 60);
		circ.paint(g);

		Driehoek driehoek = new Driehoek(
			new Punt(100, 100),
			new Punt(200, 100),
			new Punt(100, 200) );
		driehoek.paint(g);
	}

}
