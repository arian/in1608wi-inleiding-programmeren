//	Practicum IN1608WI	Opdracht 8
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 17 11 2011

package opdracht8;

import javax.swing.JFrame;

public class MaakTekening1 extends JFrame {

	public static void main(String[] argv) {

		Cirkel circ = new Cirkel(new Punt(40, 40), 60);

		Driehoek driehoek = new Driehoek(
			new Punt(100, 100),
			new Punt(200, 100),
			new Punt(100, 200) );

		MKVormLijst lijst = new MKVormLijst();
		lijst.voegToe(circ);
		lijst.voegToe(driehoek);

		Tekenscherm1 tekenscherm = new Tekenscherm1(lijst);
	}

}
