//	Practicum IN1608WI	Opdracht 4
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht4;

public class Opdracht4_1 {

	public static void main(String[] argv){
		Punt p1 = new Punt(1.0, 2.0);

		System.out.println("x-coordinaat van p1: " + p1.getX());
		System.out.println("y-coordinaat van p1: " + p1.getY());

		System.out.println("String representatie van p1: " + p1);

		Punt p2 = new Punt(2.0, 3.0);

		System.out.println("String representatie van p2: " + p2);

		System.out.println("p1.equals(p1)   = " + p1.equals(p1));
		System.out.println("p1.equals(p2)   = " + p1.equals(p2));
		System.out.println("p1.equals(null) = " + p1.equals(null));
		System.out.println("p1.afstand(p2)  = " + p1.afstand(p2));

		System.out.println("p1.transleer(1.0, 1.0)");
		p1.transleer(1.0, 1.0);

		System.out.println("p1.equals(p2)   = " + p1.equals(p2));
		System.out.println("p1.afstand(p2)  = " + p1.afstand(p2));

	}

}
