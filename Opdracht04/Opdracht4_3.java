//	Practicum IN1608WI	Opdracht 4
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht4;

public class Opdracht4_3 {

	public static void main(String[] argv){

		// deel 3
		System.out.println("\nDeel 3\n");

		Cirkel c1 = new Cirkel(new Punt(1.0, 2.0), 1.0);

		System.out.println("Middelpunt c1: " + c1.getMiddelpunt());
		System.out.println("Straal c1:" + c1.getStraal() + "\n");

		Cirkel c2 = new Cirkel(new Punt(1.0, 2.0), -7.0);

		System.out.println("Middelpunt c2: " + c2.getMiddelpunt());
		System.out.println("Straal c2:" + c2.getStraal() + "\n");

		System.out.println("String representatie van c1: " + c1);
		System.out.println("String representatie van c2: " + c2 + "\n");

		// deel 4
		System.out.println("\nDeel 4\n");

		c2 = new Cirkel(new Punt(3.0, 4.0), 1.0);

		System.out.println("String representatie van c1: " + c1);
		System.out.println("String representatie van c2: " + c2);

		System.out.println("c1.equals(c1) is " + c1.equals(c1));
		System.out.println("c1.equals(c2) is " + c1.equals(c2) + "\n");

		System.out.println("Omtrek van c1: " + c1.omtrek());
		System.out.println("Oppervlakte van c1: " + c1.oppervlakte() + "\n");

		System.out.println("c1.overlapt(c2) is " + c1.overlapt(c2));

		c1.transleer(1.0, 1.0);
		System.out.println("c1.transleer(1.0, 1.0)");

		System.out.println("String representatie van c1: " + c1);
		System.out.println("c1.overlapt(c2) is " + c1.overlapt(c2));

	}

}
