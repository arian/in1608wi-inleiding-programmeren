//	Practicum IN1608WI	Opdracht 4
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht4;

public class Opdracht4_5 {

	public static void main(String[] argv){

		// Deel 5
		Punt p1 = new Punt(0.0, 0.0);
		Punt p2 = new Punt(1.0, 0.0);
		Punt p3 = new Punt(0.0, 1.0);

		Driehoek d1 = new Driehoek(p1, p2, p3);

		System.out.println("Punt p1: " + p1);
		System.out.println("Punt p2: " + p2);
		System.out.println("Punt p3: " + p3);

		System.out.println("String representatie van d1: " + d1 + "\n");

		// Deel 6

		Punt p4 = new Punt(4.0, 4.0);
		Punt p5 = new Punt(5.0, 4.0);
		Punt p6 = new Punt(4.0, 5.0);
		Driehoek d2 = new Driehoek(p4, p5, p6);

		System.out.println("String representatie van d2: " + d2);

		System.out.println("Omtrek van d1: " + d1.omtrek());
		System.out.println("Oppervlakte van d1: " + d1.oppervlakte() + "\n");

		System.out.println("d1.equals(d1) is " + d1.equals(d1));
		System.out.println("d1.equals(d2) is " + d1.equals(d2));

		d1.transleer(4.0, 4.0);
		System.out.println("d1.transleer(4.0, 4.0)");

		System.out.println("d1.equals(d2) is " + d1.equals(d2));

	}

}
