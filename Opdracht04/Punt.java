//	Practicum IN1608WI	Opdracht 4
//	Auteur Arian Stolwijk,	Studienummer 4001079
//	Datum 12 10 2011

package opdracht4;

public class Punt {

	private double x, y;

	/**
	 * Constructs Punt object
	 * @param _x
	 * @param _y
	 */
	Punt(double _x, double _y){
		x = _x;
		y = _y;
	}

	/**
	 * Getx the x coordinate
	 */
	public double getX(){
		return x;
	}

	/**
	 * Gets the y coordinate
	 */
	public double getY(){
		return y;
	}

	/**
	 * Translates the point with dx and dy
	 * @param dx
	 * @param dy
	 */
	public void transleer(double dx, double dy){
		x += dx;
		y += dy;
	}

	/**
	 * Calculates the distance to another point
	 * @param that
	 */
	public double afstand(Punt that){
		double dx = that.x - x;
		double dy = that.y - y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Checks if the other object equals this object
	 * @param other
	 */
	@Override
	public boolean equals(Object other){
		if (other instanceof Punt){
			Punt p = (Punt) other;
			return p.x == x && p.y == y;
		}
		return false;
	}

	/**
	 * String representation of this object
	 */
	@Override
	public String toString(){
		return "<Punt(" + x + "," + y + ")>";
	}

}
